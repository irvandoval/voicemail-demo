const express = require('express');
const axios = require('axios');
const cors = require('cors');

const app = express();
const serverUrl = 'https://sandbox.2600hz.com:8443/v2';

app.use(cors())

app.get('/', (_, res) => res.send('Server is up!!'));

/**
 * Get messages from kazoo api
 * @memberof messages
 * @function
 * @name messages
 */
app.get('/accounts/:accountId/vmboxes/:vmBoxId/messages', async ({ headers: { authorization }, params }, res) => {
  const { vmBoxId, accountId } = params;

  if (authorization && accountId && vmBoxId) {
    try {
      const { data } = await axios.get(`${serverUrl}/accounts/${accountId}/vmboxes/${vmBoxId}/messages`, {
        headers: { Authorization: authorization },
      });
      res.json({ ...data });
    } catch (error) {
      res.status(500).json({message: 'error retrieving data', ...error });
    }
  } else {
    res.status(400).json({ message: 'bad params error' })
  }
});

app.listen(3001, () => console.log('Example app listening on port 3001!'));
