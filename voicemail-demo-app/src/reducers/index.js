import { combineReducers } from 'redux';
import voicemailMessages from './voicemailMessages';

export default combineReducers({
  voicemailMessages,
});
