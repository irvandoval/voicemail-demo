/**
 * Reducer that handles voice mails managent view
 */
import { 
  FETCH_VOICEMAIL_MESSAGES_SUCCESS,
  FETCH_VOICEMAIL_MESSAGES_FAILURE,
  FETCH_VOICEMAIL_MESSAGES,
  SET_SNACKBAR,
} from "../actions/constants";

const defaultState = {
  snackbar: {
    open: false,
    message: '',
  },
  isLoading: false,
  data: {
    data: [],
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case SET_SNACKBAR:
      return { ...state, snackbar: payload };
    case FETCH_VOICEMAIL_MESSAGES:
      return { ...state, isLoading: true };
    case FETCH_VOICEMAIL_MESSAGES_SUCCESS:
    case FETCH_VOICEMAIL_MESSAGES_FAILURE:
      return { ...state, data: payload, isLoading: false }
    default:
      return state;
  }
}
