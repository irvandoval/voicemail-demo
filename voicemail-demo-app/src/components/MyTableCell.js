import React, { Fragment } from 'react';
import PropTypes from 'prop-types'
import { TableCell } from '@material-ui/core';
import { parseTime, parsePhoneNumber } from '../utils';


/**
 * Component for showing Table Cell with data
 * 
 * @component
 * @example
 * const data =  134567543:
 * const type = 'time';
 * return (
 *   <MyTableCell data={data} type={type}/>
 * )
 */
const MyTableCell = ({ data, type, ...props }) => {
  let info;
  switch (type) {
    case 'from':
      info = data.from.includes('anonymous')
        ? 'Anonymous'
        : data.idName.includes('+')
          ? parsePhoneNumber(
            data.idName.split('@')[0]
          )
          : data.idName;
      break;
    case 'to':
      info = data.includes('+')
        ? parsePhoneNumber(
          data.split('@')[0]
        )
        : data;
      break;
    case 'time':
      info = parseTime(data);
      break;
    case 'text':
      info = data;
      break;
    default:
      info = '';
      break;
  }

  return (<Fragment>
    {(
      <TableCell align="right" {...props}>{info}</TableCell>
    )}
  </Fragment>)
};

MyTableCell.propTypes = {
  data: PropTypes.any.isRequired,
  type: PropTypes.string.isRequired,
}

export default MyTableCell;
