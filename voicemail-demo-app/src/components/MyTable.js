import React from 'react';
import PropTypes from 'prop-types'
import Table from '@material-ui/core/Table';
import MyTableCell from './MyTableCell';
import {
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

/**
 * Component for showing Table with data
 * 
 * @component
 * @example
 * const data = [{timestamp: 134567543, from: '123@s.com', to: '122@qwerty.com', folder: 'deleted' }]
 * return (
 *   <MyTable data={data}/>
 * )
 */
const MyTable = ({ data, columns }) => {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="my table">
        <TableHead>
          <TableRow>
            <MyTableCell type="text" data="Status"/>
            <MyTableCell type="text" data="From"/>
            <MyTableCell type="text" data="To"/>
            <MyTableCell type="text" data="Duration"/>
          </TableRow>
        </TableHead>
        <TableBody>
        {data.map((row, index) => (
            <TableRow key={index}>
               <MyTableCell type="text" data={row.folder} component="th" scope="row"/>
               <MyTableCell type="from" data={{from: row.from, idName: row['caller_id_name']}}/>
               <MyTableCell type="to" data={row.to}/>
               <MyTableCell type="time" data={row.length}/>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

MyTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    folder: PropTypes.oneOf(['new', 'saved', 'deleted']),
    from: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
    timestamp: PropTypes.number.isRequired,
  })),
}

export default MyTable;