import React, { Component } from 'react';
import { IconButton, Snackbar, AppBar, Toolbar, Typography, CircularProgress } from '@material-ui/core';
import { bindActionCreators } from 'redux';
import CloseIcon from '@material-ui/icons/Close';
import { connect } from 'react-redux';
import {
  fetchVoicemailMessages,
  setSnackbar,
} from '../actions/voicemailMessages';
import MyTable from '../components/MyTable';

/**
 * App container
 */
class App extends Component {
  componentDidMount() {
    const { fetchVoicemailMessages } = this.props;

    fetchVoicemailMessages();
  }

  componentDidUpdate(prevProps) {
    const { voicemailMessages: { data: { status } } } = this.props;

    if (status !== prevProps.voicemailMessages.data.status && status === 'error') {
      this.openMessage('Error retrieving data');
    }
  }

  /**
   * Open snackbar with a message
   * @param {String} message - Message to show in the snackbar.
   */
  openMessage = (message) => {
    const { setSnackbar } = this.props;

    setSnackbar({ open: true, message });
  }

  /**
   * Close snackbar
   */
  closeMessage = () => {
    const { setSnackbar } = this.props;

    setSnackbar({ open: false });
  }

  render = () => {
    const { voicemailMessages } = this.props;
    const { snackbar: { open, message }, data: { data }, isLoading } = voicemailMessages;
    
    return (<>
      <AppBar position="sticky">
        <Toolbar>
          { isLoading && (<CircularProgress color="secondary" />)}
          <Typography variant="h6" >
            Voicemail messages demo
          </Typography>
        </Toolbar>
      </AppBar>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={open}
        autoHideDuration={6000}
        onClose={this.closeMessage}
        message={message}
        action={
          <IconButton size="small" aria-label="close" color="inherit" onClick={this.closeMessage}>
            <CloseIcon fontSize="small" />
          </IconButton>
        }
      />

      <MyTable data={data} />
    </>)
  }
}

const mapStateToProps = ({
  voicemailMessages,
}) =>
  ({ voicemailMessages });

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    fetchVoicemailMessages,
    setSnackbar,
  },
  dispatch);



export default connect(mapStateToProps, mapDispatchToProps)(App);
