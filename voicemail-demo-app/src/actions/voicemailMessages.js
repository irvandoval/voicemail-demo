import {
  FETCH_VOICEMAIL_MESSAGES,
  FETCH_VOICEMAIL_MESSAGES_SUCCESS,
  FETCH_VOICEMAIL_MESSAGES_FAILURE,
  SET_SNACKBAR
} from "./constants";

export const setSnackbar = (payload) => ({ type: SET_SNACKBAR, payload });

export const fetchVoicemailMessages = () => ({ type: FETCH_VOICEMAIL_MESSAGES });

export const fetchVoicemailSuccess = (payload) => ({ type: FETCH_VOICEMAIL_MESSAGES_SUCCESS, payload });

export const fetchVoicemailFailure= (payload) => ({ type: FETCH_VOICEMAIL_MESSAGES_FAILURE, payload });

