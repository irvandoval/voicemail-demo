import { fork } from 'redux-saga/effects';
import voicemailMessagesSaga from './voicemailMessagesSaga';

export default function* root() {
  yield [
    fork(voicemailMessagesSaga),
  ];
}
