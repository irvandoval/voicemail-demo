import { call, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { fetchVoicemailSuccess, fetchVoicemailFailure } from '../actions/voicemailMessages';
import { FETCH_VOICEMAIL_MESSAGES } from '../actions/constants';
const serverUrl = 'http://localhost:3001';
const credentials = 'NDY0MmU2NDA0MGNkYjhiODljMzEwYTIxYTA3YzdmNjI6MjMyNjQxNTY1OTA3NWU3NTAwMGNlY2Q3YmNiZjM3NTY=';
const accountId = '4642e64040cdb8b89c310a21a07c7f62'
const vmBoxId = 'b37675a2d7b90d60f0ee5d4175502394'
const headers = {
  Authorization: `Basic ${credentials}`
};

function getVoicemailMessages() {
  const url = `${serverUrl}/accounts/${accountId}/vmboxes/${vmBoxId}/messages`
  return axios.get(url, { headers });
}

function* fetchMessages () {
  try {
    const { data } = yield call(getVoicemailMessages);
    yield put(fetchVoicemailSuccess(data));
  } catch (error) {
    yield put(fetchVoicemailFailure({ status: 'error', ...error }));
  }
}

export default function* voicemailMessagesSaga() {
  yield [
    takeEvery(FETCH_VOICEMAIL_MESSAGES, fetchMessages),
  ]
}
