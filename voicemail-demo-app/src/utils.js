import { parsePhoneNumberFromString } from 'libphonenumber-js';

export const parsePhoneNumber = (str) =>
  parsePhoneNumberFromString(str).formatInternational();


/**
 * Parse time in ms to readable time
 * @param {*} time - time in ms
 */
  export const parseTime = time => {
  const milliseconds = parseInt((time % 1000) / 100);
  let seconds = Math.floor((time / 1000) % 60);
  let minutes = Math.floor((time / (1000 * 60)) % 60);
  let hours = Math.floor((time / (1000 * 60 * 60)) % 24);

  hours = (hours < 10) ? "0" + hours : hours;
  minutes = (minutes < 10) ? "0" + minutes : minutes;
  seconds = (seconds < 10) ? "0" + seconds : seconds;

  return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
}
